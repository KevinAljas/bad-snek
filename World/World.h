#ifndef WORLD_H
#define WORLD_H

#define WORLD_SIZE 20

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <ncurses.h>

struct Snake 
{
    int x[WORLD_SIZE * WORLD_SIZE];
    int y[WORLD_SIZE * WORLD_SIZE];
    int size;
    int speed;

    int direction;

    int ghost_x;
    int ghost_y;
};

struct Apple
{
    int x;
    int y;
};


class World
{
    public:
    char grid[WORLD_SIZE][WORLD_SIZE];
    Snake snake = {{3}, {3}, 1, 500, 4, 2, 3};
    Apple apple;

    void PrintBorder();
    void PrintWorld();
    void PrintEnd();

    void AddSnake();
    void MoveSnake();
    void GrowSnake();
    
    bool CheckCollision();

    void SpawnApple();
    void CheckEat(); 

    void SetColor(char tile);
};

#endif // WORLD_H


output: Snek.o World.o
	g++ Snek.o World.o -o Snek -lncurses
	@make clean

Snek.o: Snek.cpp
	g++ -c Snek.cpp -lncurses

World.o: World/World.cpp World/World.h
	g++ -c World/World.cpp -lncurses

clean:
	@rm -f *.o


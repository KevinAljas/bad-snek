#include <iostream>
#include <time.h>
#include <stdlib.h>

#include <ncurses.h>
#include "./World/World.h"

using namespace std;

// Function prototypes
void Delay(int milliseconds);
unsigned long Millis();
int Controls(int key_code);

// Main gameloop
int main(void)
{
    // Initialize world
    World world;
    
    initscr();
    noecho();
    srand(time(NULL));

    //resizeterm(100, 50);

    // Not really portable!
    start_color();
    curs_set(0);

    // Set colors 
    init_pair(1, COLOR_WHITE, COLOR_WHITE); // Background
    init_pair(2, COLOR_GREEN, COLOR_GREEN); // Snake
    init_pair(3, COLOR_BLACK, COLOR_GREEN); // Snake head
    init_pair(4, COLOR_RED, COLOR_RED); // Apple
    init_pair(5, COLOR_MAGENTA, COLOR_MAGENTA); // Wall
    init_pair(6, COLOR_BLACK, COLOR_WHITE); // Text

    world.SpawnApple(); 
    
    // Game loop 
	while(1)
    {         
        // Snake stuff    
        world.MoveSnake(); 
        if (world.CheckCollision()) break;
       
        // Render
        world.PrintBorder();
        world.PrintWorld();	
        
        // Start frame timer
        int frame_start = Millis();
         
        // Read keyboard input until next frame 
        nodelay(stdscr, TRUE);
        int new_command = world.snake.direction;
        while (Millis() - frame_start < world.snake.speed) 
        {
            int key_press = getch();
            int command = Controls(key_press);
            if ((command) && (((command + world.snake.direction) & 1)) || (command == world.snake.direction)) 
               new_command = command; 
        }
        
        world.snake.direction = new_command;
        nodelay(stdscr, FALSE);
    }
        
    // End screen
    world.PrintEnd();
    while(getch() != 'q');
    endwin();
    return 0;
}

// Functions
void Delay(int milliseconds)
{
    milliseconds *= CLOCKS_PER_SEC / 1000;
    clock_t start = clock();
    while (clock() - start < milliseconds);
    return;
}

unsigned long Millis()
{
    static unsigned long milliseconds = 0;
    milliseconds = (double) clock() / (double) CLOCKS_PER_SEC * 1000;
    return milliseconds;
} 

int Controls(int key_code)
{
    switch (key_code)
    {
        case 'w':
            return 1;
        
        case 'a':
            return 2;
        
        case 's':
            return 3;
        
        case 'd':
            return 4;
    
        default:
            return 0;
    }
}
